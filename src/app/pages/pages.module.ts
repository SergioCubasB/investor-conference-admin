import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../shared/material/material.module';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SearchFilterPipe } from "../pipes/search-filter.pipe";
import { BingoCardComponent } from './bingoCard/bingoCard.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ModulesComponent } from './dashboard/modules/modules.component';

@NgModule({
  declarations: [
    PagesComponent,
    BingoCardComponent,

    SearchFilterPipe,
      DashboardComponent,
      ModulesComponent

  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,

    AppRoutingModule,
    SharedModule,

    PagesRoutingModule,

    IonicModule.forRoot()
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class PagesModule { }
