import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";



/* Componentes principales */
import { BingoCardComponent } from "./bingoCard/bingoCard.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ModulesComponent } from "./dashboard/modules/modules.component";

const routes: Routes = [
    { 
        path: 'cartilla', 
        component: BingoCardComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'module', component: ModulesComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule {}