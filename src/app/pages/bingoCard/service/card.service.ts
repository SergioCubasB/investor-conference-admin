import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  private env = environment;
  protected endpoint: string;

  constructor(
    private http: HttpClient
  ) { 
    this.endpoint = `${environment.api}/v1`;
  }

  getCardUser(){
    const token = localStorage.getItem("_TK");
    
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    })

    return this.http.get(this.endpoint + '/user/ver-cartilla', { headers });
  }

  getCardUserDownload(id:string){
    const token = localStorage.getItem("_TK");
    
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    })

    return this.http.get(this.endpoint + '/user/search-card?id='+id, { headers, responseType: 'blob' });
  }

}
