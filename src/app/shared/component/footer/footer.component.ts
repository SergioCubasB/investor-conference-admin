import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: 
  `<footer> <p>COPYRIGHT CREDICORP CAPITAL. TODOS LOS DERECHOS RESERVADOS</p> </footer>`,
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
