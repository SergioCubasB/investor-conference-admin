import { Component, Input, OnInit } from '@angular/core';
import { CardService } from 'src/app/pages/bingoCard/service/card.service';
import * as fileSaver from 'file-saver';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() id: string = '-';

  @Input() b1: string = '-';
  @Input() b2: string = '-';
  @Input() b3: string = '-';
  @Input() b4: string = '-';
  @Input() b5: string = '-';

  @Input() i1: string = '-';
  @Input() i2: string = '-';
  @Input() i3: string = '-';
  @Input() i4: string = '-';
  @Input() i5: string = '-';

  @Input() n1: string = '-';
  @Input() n2: string = '-';
  @Input() n4: string = '-';
  @Input() n5: string = '-';

  @Input() g1: string = '-';
  @Input() g2: string = '-';
  @Input() g3: string = '-';
  @Input() g4: string = '-';
  @Input() g5: string = '-';

  @Input() o1: string = '-';
  @Input() o2: string = '-';
  @Input() o3: string = '-';
  @Input() o4: string = '-';
  @Input() o5: string = '-';


  isLoading: boolean = false;
  constructor(
    private cardService: CardService
  ) { }

  ngOnInit(): void {
  }

  downloadCard(id: string){
    this.isLoading = true;
    this.cardService.getCardUserDownload(id)
    .subscribe(
      (data: any) => {
        let blob:any = new Blob([data], { type: 'text/json; charset=utf-8' });
        const url = window.URL.createObjectURL(blob);
        fileSaver.saveAs(blob, 'cartilla.pdf');
        this.isLoading = false;
        
      }
    )
  }

}
