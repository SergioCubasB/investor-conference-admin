import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-banner',
  template: `
    <div class="container-banner" 
    [ngStyle]="{'background': type === 'image' ? 'url('+ image +')' : color}">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  @Input() image: string = "https://google.com.pe";
  @Input() color: string = "red";

  @Input() type: string = "image";

  @Input() orientation: string = "horizontal";

  constructor() { }

  ngOnInit(): void {
    console.log(this.type);
    console.log(this.image);
    console.log(this.orientation);
  }

}
