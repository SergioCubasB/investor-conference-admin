import { DoBootstrap, Inject, Injector, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { MaterialModule } from "./material/material.module";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { createCustomElement } from "@angular/elements";
import { LoaderComponent } from './component/loader/loader.component';
import { HeaderComponent } from './component/header/header.component';
import { CardComponent } from './component/card/card.component';
import { BannerComponent } from './component/banner/banner.component';
import { FooterComponent } from './component/footer/footer.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';


@NgModule({
  declarations: [ 
    LoaderComponent, 
    HeaderComponent, 
    CardComponent, BannerComponent, FooterComponent, SidebarComponent
  ],
  imports: [
      BrowserAnimationsModule,
      MaterialModule,
      BrowserModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,

      IonicModule.forRoot()
  ],
  exports: [
    LoaderComponent,
    HeaderComponent,
    CardComponent,
    BannerComponent,
    FooterComponent,
    SidebarComponent
  ]
})
export class SharedModule{}