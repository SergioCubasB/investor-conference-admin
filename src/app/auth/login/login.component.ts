import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Auth } from '../interface/auth';
import { AuthService } from '../service/auth.service';
import { LocalStorageService } from '../service/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{
  
  frmLogin = new FormGroup({
    document: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  isLoading: boolean = true;
  buttonClicked$ = false;

  constructor( 
    private formBuilder: FormBuilder, 
    public dialog: MatDialog,
    private router: Router,

    private authService: AuthService,
    private localStorageService: LocalStorageService
    ) {
    }

  ngOnInit(): void {
    localStorage.clear();

    setTimeout(() => {
      this.isLoading = false;
    }, 2000);
  }

  get f(){
    return this.frmLogin.controls;
  }

  validarUsuario() {
    this.router.navigateByUrl('dashboard/module');
    
    const formData = new FormData();

    const { document } = this.frmLogin.value;

    formData.append('user', document)
    formData.append('password', document)

    
    if(document.trim() != ''){
      this.buttonClicked$ = true;

      this.authService.postLogin(formData)
      .subscribe(
        (auth : any) => {
          this.buttonClicked$ = false;
          const { status, data } = auth.data;
          
          if(status === 200){
            this.localStorageService.set("_TK",data.tk);

            this.router.navigateByUrl('cartilla');
          }else{
            Swal.fire({
              icon: 'warning',
              title: 'Usuario invalido...',
              text: 'El documento ingresado no se encuentra registrado.',
            })
          }
          
        }
      )
    }
   
  }


}
